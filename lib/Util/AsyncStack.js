'use strict';

/**
 * Class for holding and managing a call stack
 */
class CallStack {
	/**
	 * Creates a new CallStack object
	 * @param {string} stack A stack from new Error().stack
	 */
	constructor(stack) {
		this._origStack = stack;
		this._stack = null;
	}

	init() {
		const stackLines = this._origStack.split('\n');

		// Removes "Error", the line added by "new Error().stack" and the line that called "captureStack"
		stackLines.splice(0, 3);

		this._stack = stackLines;
	}

	/**
	 * Appends lines to this call stack
	 * The function will stop appending as soon as it finds a line that matches the top element
	 * This is to avoid adding already existing stack lines
	 *
	 * @param {string[]} stack The lines that should be appended
	 */
	append(stack) {
		if (this._stack === null) this.init();
		const stackLines = stack.split('\n');

		// Removes "Error"
		stackLines.shift();

		const elementZero = this._stack[0];
		const appendLines = [];
		let i = 0;
		let stackLine = stackLines[i];
		while (stackLine !== elementZero && stackLine !== undefined) {
			appendLines.push(stackLine);
			stackLine = stackLines[++i];
		}

		this._stack = appendLines.concat(this._stack);
	}

	/**
	 * Execute this on the beginning of a function
	 * This pushes the line that called your function to the call stack
	 */
	push() {
		if (this._stack === null) this.init();
		const errorStack = new Error().stack;
		const stackLines = errorStack.split('\n');

		// Removes "Error", the line added by "new Error().stack" and the line that called "push"
		stackLines.splice(0, 3);

		const call = stackLines[0];
		this._stack.unshift(call);
	}

	/**
	 * Call at the end of a function
	 * This pops the line of the stack that called your function
	 */
	pop() {
		if (this._stack === null) this.init();
		this._stack.shift();
	}

	/**
	 * Returns a copy of this stack
	 * @returns {Object} A copy of this stack
	 */
	clone() {
		const cs = new CallStack(this._origStack);
		cs._stack = this._stack.slice(0);
		return cs;
	}

	/**
	 * Applies this CallStack onto an Error and (optionally) merges the old error stack
	 *
	 * @param {Error} error The error where this CallStack should be applied to
	 * @param {boolean=} noMerge set to truly value if the existing stack on the error should not be merged
	 * @returns {Error} The input argument "error"
	 */
	applyOn(error, noMerge) {
		if (this._stack === null) this.init();
		let stack = this._stack;
		if (!noMerge) {
			const clone = this.clone();
			clone.append(error.stack);
			stack = clone._stack;
		}
		error.stack = `${error.stack.split('\n')[0]}\n${stack.join('\n')}`;
		return error;
	}
}

/**
 * Captures the current call stack in an object
 * @returns {Object} A CallStack object that contains the current call stack
 */
module.exports.captureStack = () => new CallStack(new Error().stack);
