'use strict';

const LibraryClient = require('ioredis');

const url = require('url');
const { EventEmitter } = require('events');

/**
 * Example DSN: redis://127.0.0.1:6379/?auth=secret&prefix=my.cool.prefix&db=2
 */
class RedisClient extends EventEmitter {
	constructor(dsn, opts) {
		super();

		// defaults
		this.opts = Object.assign({}, {
			prefix: null,
			prefixPolicy: false,
			stringNumbers: true,
			enableOfflineQueue: false,
		}, opts);

		// parse redis dsn
		const parsed = url.parse(dsn, true);

		if (!['hostname', 'port'].every(i => parsed[i])) throw new Error(`DSNError: Please make sure you've provided a valid DSN`);
		this.host = parsed.hostname;
		this.port = parsed.port;

		// get dsn query options
		if (parsed.query) {
			['auth', 'db', 'prefix'].forEach(i => {
				this[i] = parsed.query[i] || null;
			});
		}

		// format prefix
		if (this.prefix) {
			if (this.prefix.slice(-1) === '.') this.prefix = this.prefix.slice(0, -1);
		}

		this.cli = null;
	}

	async init() {
		const clientOptions = Object.assign({}, this.opts, {
			host: this.host,
			port: this.port,
			db: this.db,
			password: this.auth,
		});

		// init client
		this.cli = new LibraryClient(clientOptions);

		['ready', 'connect', 'disconnect', 'reconnecting', 'error'].forEach(i => {
			this.cli.on(i, (...args) =>	this.emit(i, args));
		});
	}

	parseOpts(opts) {
		return Object.assign({}, {
			prefix: this.opts.prefixPolicy,
			json: false,
		}, opts);
	}

	async get(k, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}.${k}`;

		const resp = await this.cli.get(k);

		if (opts.json) return JSON.parse(resp);

		return resp;
	}

	async set(k, v, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}.${k}`;

		if (opts.json && typeof v !== 'string') v = JSON.stringify(v);

		return this.cli.set(k, v);
	}

	async setex(k, s, v, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}.${k}`;

		if (opts.json && typeof v !== 'string') v = JSON.stringify(v);

		return this.cli.setex(k, s, v);
	}

	async incr(k, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}.${k}`;

		return this.cli.incr(k);
	}

	async llen(k, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}.${k}`;

		return this.cli.llen(k);
	}

	async lpop(k, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}.${k}`;

		return this.cli.lpop(k);
	}

	async lpush(k, vals, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}.${k}`;

		return this.cli.lpush(k, vals);
	}

	async lrange(k, start, stop, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}.${k}`;

		return this.cli.lrange(k, start, stop);
	}

	async ttl(k, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}.${k}`;

		return this.cli.ttl(k);
	}

	async expire(k, s, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}.${k}`;

		return this.cli.expire(k, s);
	}
}

module.exports = RedisClient;
