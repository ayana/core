'use strict';

const { EventEmitter } = require('events');

/**
 * Helper class to make listening and unregistering listeners from eventemitters a simple process.
 */
class EventListener extends EventEmitter {
	constructor(errorHandler) {
		super();

		if (!errorHandler) throw new Error('EventListener was initialized without an errorHandler');
		this.errorHandler = errorHandler;

		this.listeners = [];
	}

	/**
	 * Listen for events on provided emitter and send them to provided asyncListener
	 *
	 * @param {EventEmitter} emitter Emitter to listen on
	 * @param {string} event Event to listen for
	 * @param {function} asyncListener where to send heard events
	 * @returns {function} wrapped listener for unregister
	 */
	asyncRegister(emitter, event, asyncListener) {
		const syncListener = (...args) => asyncListener(...args).catch(e => this.errorHandler(e, event, emitter));
		return this.register(emitter, event, syncListener);
	}

	/**
	 * Listen for events on provided emitter and send them to provided listener
	 *
	 * @param {EventEmitter} emitter Emitter to listen on
	 * @param {string} event Event to listen for
	 * @param {function} listener where to send heard events
	 * @returns {function} listener
	 */
	register(emitter, event, listener) {
		emitter.on(event, (...args) => {
			try {
				listener(...args);
			} catch (e) {
				this.errorHandler(e, event, emitter);
			}
		});
		this.listeners.push({
			emitter,
			event,
			listener,
		});

		return listener;
	}

	/**
	 * Listen for events on provided emitter and send them to asyncListener, stops listening after first event
	 *
	 * @param {EventEmitter} emitter Emitter to listen on
	 * @param {string} event Event to listen for
	 * @param {function} asyncListener where to send heard event
	 */
	asyncRegisterOnce(emitter, event, asyncListener) {
		const syncListener = (...args) => asyncListener(...args).catch(e => this.errorHandler(e, event, emitter));
		this.registerOnce(emitter, event, syncListener);
	}

	/**
	 * Listen for events on provided emitter and send them to listener, stops listening after first event
	 *
	 * @param {EventEmitter} emitter Emitter to listen on
	 * @param {string} event Event to listen for
	 * @param {function} listener where to send heard event
	 * @returns {function} wrapped listener for register
	 */
	registerOnce(emitter, event, listener) {
		const onceListener = (...args) => {
			this.unregister(emitter, event, onceListener);
			listener(...args);
		};

		return this.register(emitter, event, onceListener);
	}

	/**
	 * Stop listening for event on provided emitter
	 * @param {EventEmitter} emitter Emitter to stop listening on
	 * @param {string} event Event to stop listening to
	 * @param {function} listener Initial function
	 */
	unregister(emitter, event, listener) {
		emitter.removeListener(event, listener);

		this.listeners = this.listeners.reduce((a, d) => {
			if (d.emitter !== emitter || d.event !== event || d.listener !== listener) a.push({ emitter: d.emitter, event: d.event, listener: d.listener });
			return a;
		}, []);
	}

	/**
	 * Unregister all event listeners from all emitters
	 */
	unregisterAll() {
		this.listeners.forEach(({ emitter, event, listener }) => {
			emitter.removeListener(event, listener);
		});

		this.listeners = [];
	}
}

module.exports = EventListener;
