'use strict';

const log = require('@ayana/logger').get('Redis');

const url = require('url');
const { EventEmitter } = require('events');

const LibraryClient = require('ioredis');

/**
 * Example DSN: redis://127.0.0.1:6379/?auth=secret&prefix=my.cool.prefix&db=2
 */
class Redis extends EventEmitter {
	/**
	 * Creates a new Redis client
	 *
	 * @param {string} dsn The DSN of the client (ex. redis://127.0.0.1:6379/?auth=secret&prefix=my.cool.prefix&db=2)
	 * @param {object} options The options
	 * @param {string=} options.appPrefix The prefix for every key defined by the application. (Default: null)
	 * @param {boolean=} options.allowDsnPrefix Allows or disallows a prefix in the DSN (Default: true)
	 */
	constructor(dsn, options) {
		super();

		// Apply default options
		options = Object.assign({
			appPrefix: null,
			allowDsnPrefix: false,
		}, options);

		// Parse Redis DSN
		const parsed = url.parse(dsn, true);

		if (!['hostname', 'port'].every(i => parsed[i] != null)) throw new Error(`DSNError: Please make sure you've provided a valid DSN`);

		// Define hostname
		Object.defineProperty(this, 'host', {
			value: parsed.hostname,
			writable: false,
			configurable: false,
			enumerable: true,
		});

		// Define port
		Object.defineProperty(this, 'port', {
			value: parsed.port,
			writable: false,
			configurable: false,
			enumerable: true,
		});

		// Define database
		Object.defineProperty(this, 'db', {
			value: parsed.query.db || null,
			writable: false,
			configurable: false,
			enumerable: true,
		});

		let prefix = '';

		// Apply DSN prefix
		if (parsed.query.prefix != null && options.allowDsnPrefix) {
			prefix += parsed.query.prefix;
			// Apply dot if not present
			if (prefix.slice(-1) !== '.') prefix = `${prefix}.`;
		}

		// Apply application prefix
		if (options.appPrefix != null) {
			prefix += options.appPrefix;
			// Append dot if not present
			if (prefix.slice(-1) !== '.') prefix = `${prefix}.`;
		}

		// Define prefix
		Object.defineProperty(this, 'prefix', {
			value: prefix,
			writable: false,
			configurable: false,
			enumerable: true,
		});

		const cli = new LibraryClient({
			host: this.host,
			port: this.port,
			db: this.db,
			password: parsed.query.auth || null,

			// Don't connect directly
			lazyConnect: true,
			// Only fires the ready event when the server is actually ready
			enableReadyCheck: true,
		});

		// Define client
		Object.defineProperty(this, 'cli', {
			value: cli,
			writable: false,
			configurable: false,
			enumerable: false,
		});

		// Register logging events
		this.cli.on('connect', () => {
			log.debug('Connected', `${this.host}:${this.port}`);
		});
		this.cli.on('ready', () => {
			log.info('Ready', `${this.host}:${this.port}`);
		});
		this.cli.on('close', () => {
			log.info('Closed', `${this.host}:${this.port}`);
		});
		this.cli.on('reconnecting', ms => {
			log.info(`Reconnecting after ${ms}ms`, `${this.host}:${this.port}`);
		});
		this.cli.on('end', () => {
			log.info('Not attempting reconnect', `${this.host}:${this.port}`);
		});

		// Forward events
		['connect', 'ready', 'error', 'close', 'reconnecting', 'end'].forEach(i => {
			this.cli.on(i, (...args) =>	this.emit(i, args));
		});
	}

	async connect() {
		return this.cli.connect();
	}

	disconnect() {
		this.cli.disconnect();
	}

	parseOpts(opts) {
		return Object.assign({
			prefix: true,
			json: false,
		}, opts);
	}

	async get(k, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix) k = `${this.prefix}${k}`;

		const resp = await this.cli.get(k);

		if (opts.json) return JSON.parse(resp);

		return resp;
	}

	async set(k, v, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix) k = `${this.prefix}${k}`;

		if (opts.json && typeof v !== 'string') v = JSON.stringify(v);

		return this.cli.set(k, v);
	}

	async setex(k, s, v, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}.${k}`;

		if (opts.json && typeof v !== 'string') v = JSON.stringify(v);

		return this.cli.setex(k, s, v);
	}

	async incr(k, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}.${k}`;

		return this.cli.incr(k);
	}

	async llen(k, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}.${k}`;

		return this.cli.llen(k);
	}

	async lpop(k, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}.${k}`;

		return this.cli.lpop(k);
	}

	async lpush(k, vals, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}.${k}`;

		return this.cli.lpush(k, vals);
	}

	async lrange(k, start, stop, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}.${k}`;

		return this.cli.lrange(k, start, stop);
	}

	async ttl(k, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}.${k}`;

		return this.cli.ttl(k);
	}

	async expire(k, s, opts) {
		opts = this.parseOpts(opts);
		if (opts.prefix && this.prefix) k = `${this.prefix}.${k}`;

		return this.cli.expire(k, s);
	}
}

module.exports = Redis;
