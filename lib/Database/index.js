'use strict';

const fs = require('fs');
const path = require('path');
const { EventEmitter } = require('events');

const Filesystem = require('../Util/FileSystem');
const { asyncAwaitForEach } = require('../Util/Async');

const { Schema } = require('mongoose');

class Database extends EventEmitter {
	constructor() {
		super();

		this.cli = require('mongoose');
		require('mongoose-long')(this.cli);

		this.cli.connection.on('error', e => this.emit('error', e));
		this.cli.connection.once('open', () => this.emit('open'));

		this.models = new Map();
	}

	init(dsn) {
		if (!dsn) throw new Error('Database: DSN Required');

		return new Promise((resolve, reject) => {
			this.cli.connect(dsn, { reconnectTries: Number.MAX_SAFE_INTEGER, reconnectInterval: 500 }, e => {
				if (e) return reject(e);

				resolve();
			});
		});
	}

	get(name) {
		return this.getModel(name);
	}

	getModel(name) {
		if (!this.models.has(name)) throw new Error(`Model '${name}' does not exist.`);
		return this.models.get(name).instance;
	}

	async registerModel(model) {
		// determine if model is a function, if so instantiate it
		if (typeof model === 'function') {
			try {
				model = new model(Schema);
			} catch (e) {
				try {
					model = model(Schema);
				} catch (e)	{
					throw new Error(`Unable to instantiate given model function. ${e}`);
				}
			}
		}

		if (!model.name || !model.schema) throw new Error('Model not in valid format.');
		if (this.models.has(model.name)) throw new Error(`Model '${model.name}' already defined.`);

		let schema = model.schema;
		if (!(schema instanceof Schema)) schema = new Schema(schema, model.opts);

		model.instance = this.cli.model(model.name, schema);
		model.instance.on('index', e => {
			if (e) return this.emit('indexError', model.name, e);

			this.emit('indexesFinish', model.name);
		});

		this.models.set(model.name, model);

		this.emit('modelCreate', model.name, model);

		return model;
	}

	async loadModels(location) {
		if (!await Filesystem.checkExists(location)) throw new Error(`Database: Non-existent models location.`);

		const items = await Filesystem.directoryListing(location);

		const models = {};
		await asyncAwaitForEach(items, async item => {
			// Fetch file stats
			if (await Filesystem.isDirectory(item)) {
				// is directory
				if (!await Filesystem.checkExists(path.resolve(item, 'index.js'))) {
					throw new Error(`Unable to load model ${item}. No index.js found`);
				}
			} else {
				// is file
				if (!/.*\.js$/i.test(item)) {
					throw new Error(`Unable to load model ${item}. Not a javascript file`);
				}
			}

			// require object / function from file
			let rawModel;
			try {
				// Delete from require cache
				delete require.cache[require.resolve(item)];
				rawModel = require(item);
			} catch (e) {
				throw new Error(`Unable to load model '${item}'. Require failed: ${e}`);
			}

			const model = await this.registerModel(rawModel);

			models[model.name] = model.instance;
		});

		return models;
	}
}

module.exports = Database;
