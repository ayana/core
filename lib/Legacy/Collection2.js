'use strict';

/**
 * Generic collection class that extends Map and provides array methods
 * @abstract Collection
 * @extends Map
 */
class Collection extends Map {
 purge() {
  this.clear();
 }
 /**
  * Convert Map to array
  * @returns {Array} Array of mapped values
  */
 toArray() {
  return [...this.values()];
 }

 /**
  * Filter values by function
  * @param {...*} args Arguments to pass to filter
  * @returns {Array.<*>}
  */
 filter(...args) {
  return this.toArray().filter(...args);
 }

 /**
  * Map values by function
  * @param {...*} args Arguments to pass to map
  * @returns {Array}
  */
 map(...args) {
  return this.toArray().map(...args);
 }

 /**
  * Reduce values by function
  * @param {...*} args Arguments to pass to reduce
  * @returns {Array}
  */
 reduce(...args) {
  return this.toArray().reduce(...args);
 }

 /**
  * Find values by function
  * @param {...*} args Arguments to pass to map
  * @returns {Array}
  */
 find(...args) {
  return this.toArray().find(...args);
 }
}

module.exports = Collection;
