'use strict';

const log = require('@ayana/logger').get('Config');

/**
 * Helper class to load and hold configuration
 */
class Config {
	/**
	 * Creates a new instance
	 *
	 * @param {string[]} vars Variables that should be loaded but are optional. Do not put required variables here.
	 * @param {string[]} required Variables that are required. Do not put optional variables here.
	 */
	constructor(vars, required) {
		this.required = required || [];
		this.vars = vars.concat(this.required);

		Object.defineProperty(this, 'cfg', {
			value: {},
			writable: false,
			enumerable: false,
			configurable: false,
		});

		Object.freeze(this.required);
		Object.freeze(this.vars);
	}

	/**
	 * Loads the configuration
	 */
	load() {
		const missing = [];
		for (const req of this.required) {
			if (Object.keys(process.env).indexOf(req) < 0) missing.push(req);
		}

		if (missing.length > 0) throw new Error(`The following config keys are missing: ${missing.join(', ')}`);

		for (const key of this.vars) {
			const value = process.env[key];

			if (value != null) {
				this.cfg[key] = value;

				// Define Property because of deprecation notice
				Object.defineProperty(this, key, {
					get: () => {
						log.warn(`Using the property access on the Config class is deprecated. Use get(key: string) instead. (K: ${key})`);
						return this.cfg[key];
					},
				});
			}
		}

		Object.freeze(this.cfg);
		Object.freeze(this);
	}

	/**
	 * Returns a config value
	 *
	 * @param {string} key The key of the value
	 *
	 * @returns {string} The value of the key
	 */
	get(key) {
		return this.cfg[key] || null;
	}
}

module.exports = Config;
