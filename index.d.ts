
declare module '@ayana/core' {
	import { EventEmitter } from 'events';

	export class Config {
		constructor(vars: string[], required?: string[]);
		load(): void;
		get(key: string): string;
	}

	export interface RedisOptions {
		appPrefix: string;
		allowDsnPrefix: boolean;
	}

	export interface RedisFunctionOptions {
		prefix?: boolean;
		json?: boolean;
	}

	export class Redis extends EventEmitter {
		constructor(dsn: string, options?: RedisOptions);
		connect(): Promise<void>;
		get(key: string, opts?: RedisFunctionOptions): Promise<string | any>;
		set(key: string, value: string | any, opts?: RedisFunctionOptions): Promise<void>;
	}
}
