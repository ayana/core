'use strict';

const { GatewayClient } = require('../../lib').Net;

const cli = new GatewayClient({ type: 'shard' });
cli.connect();

cli.on('open', d => {
 console.log(`opened connection on ${d.ip}:${d.port}`);

 // invalid
 try {
  cli.sendEvent('example', 'shard');
 } catch (e) {
  console.log('caught issue', e);
 }
});

cli.on('dispatch', pkt => {
 switch(pkt.t) {
  case "reshard":
   console.log('Reshard event!!111!1!!!11!');
   break;
 }
});

cli.on('connected', () => {
 cli.sendPKT({
  op: 0,
  t: 'anEventD00d',
  d: 'This data here is da m0st impertent dataz of all timez and needs sent after connection is 102% init',
 });

 cli.sendPKTForReply({
  op: 0,
  t: 'getShardCounts',
  d: null,
  dst: {
   t: 'shard',
   gw: true,
  },
 }).then(rpkt => {
  console.log('reply pkt: ', rpkt);
 })
 .catch(() => {
  console.log('did not get a reply pkt in time');
 });
});

cli.on('error', e => {
 console.log(e);
});

cli.on('rawWS', d => {
 console.log(JSON.stringify(d));
});
