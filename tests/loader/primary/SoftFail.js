'use strict';

class SoftFail {
	constructor(base) {
		this.base = base;

		this.dependencies = ['NotRealLul'];
	}
}

module.exports = SoftFail;
