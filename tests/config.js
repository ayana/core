'use strict';

const ConfigLoader = require('../').Config;

const cfg = new ConfigLoader(['TEST1'], ['TEST2']);

try {
	cfg.load();
} catch (e) {
	console.log('ERROR', e);
	process.exit();
}

console.log(cfg.TEST1, cfg.TEST2);
