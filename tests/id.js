const { createID } = require('../lib').Generator;

const ids = [];

for (let i = 0; i < 99999; i++) ids.push(createID());

console.log(ids.join('\n'));
