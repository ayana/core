const { TimedListener } = require('..').EmitterTools;

const { EventEmitter } = require('events');

const emitter = new EventEmitter();

TimedListener(emitter, ['test1', 'test2'], async(event, ...args) => {
	console.log(event, ...args);
	if (event === 'test2') return true;
}).catch(e => {
	console.log(e);
});

emitter.emit('test1', 'hello');
emitter.emit('test2', 'world');
